const prompt = require('prompt-sync')({ sigint: true });

let c1 = prompt('Enter the first cordinate : ');
let c2 = prompt('Enter the second cordinate : ');
c1 = Number(c1)
c2 = Number(c2)
	
	let n = 8;
	let m = 8;
	
	function findKnightMoves(chessBoard, p, q)
	{
		let X = [ 2, 1, -1, -2, -2, -1, 1, 2 ];
        let Y = [ 1, 2, 2, 1, -1, -2, -2, -1 ];

		let count = 0;

		for (let i = 0; i < 8; i++) {
			let x = p + X[i];
			let y = q + Y[i];
			if (x >= 0 && y >= 0 && x < n && y < m && chessBoard[x][y] == 0)
				count++;
		}
		return count;
	}
	
let chessBoard = [
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0]
];

console.log(findKnightMoves(chessBoard, c1, c2));

