const fs = require("fs");
const { parse } = require("csv-parse");

exports.readFile = async function (fileName) {
    let result = [];
    var promis = new Promise((resolve, reject) => {
        fs.createReadStream(fileName)
            .pipe(parse({ delimiter: ";", columns: true }))
            .on("data", (data) => {
                result.push(data);
            })
            .on("end", () => {
                resolve(result)
            })
    })
    let data = promis.then(data => {return data})
    return data

}