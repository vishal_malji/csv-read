const fs = require("fs");
const { parse } = require("csv-parse");
const { stringify } = require("csv-stringify");
let table = require("table");
const ObjectsToCsv = require('objects-to-csv')
config = {
  // Predefined styles of table
  border: table.getBorderCharacters("ramac"),
}

const prompt = require('prompt-sync')({ sigint: true });

var readCsvFile = require("./readCsvFile")

const FILE_DATA = {
  BOOKS: './books.csv',
  MAGZINES: './magzines.csv',
  AUTHOR: './authors.csv'
}

let mergedArray = []
let booksArray = []
let magzineArray = []


function filePromise(fileName) {
  let filePromise = readCsvFile.readFile(fileName).then(data => { return data })
  return filePromise
}

const getBooksAndMagezin = async () => {
  const bookPromise = filePromise(FILE_DATA.BOOKS)
  const magzinPromis = filePromise(FILE_DATA.MAGZINES)
  let prom = []
  prom.push(bookPromise)
  prom.push(magzinPromis)
  await Promise.all(prom).then(res => {

    booksArray.push(...res[0])
    magzineArray.push(...res[1])
    mergedArray.push(...res)
  })
    mergedArray = mergedArray.reduce((r, e) =>
    (r.push(...e), r), []
  )
}

function displayBooksAndMgazine() {
  console.table(mergedArray, ["title", "isbn", "authors",])
}

function findBookAndMagzineByISBN(isbn) {
  var object
  mergedArray.forEach(element => {
    if (element.isbn === isbn) {
      object = element
    }
  });
  console.table([object], ["title", "isbn", "authors", "description"])
  return true
}



async function findAllBooksAndMagzineByEmail(email) {
  var resultArray = []
  mergedArray.forEach(element => {
    console.log(element.authors)
    if (element.authors.includes(email)) {
      resultArray.push(element)
    }
  });
  console.table(resultArray, ["title", "isbn", "authors"])
}


function sortBooksAndMagzine() {
  let sortedData = mergedArray.sort((x, y) => {
    if (x.title < y.title) {
      return -1
    }
    if (x.title > y.title) {
      return 1
    }
    return 0
  })
  console.table(sortedData, ["title", "isbn", "authors"])
}


function addBook() {
  let title = prompt('Enter the Book Name : ');
  let isbn = prompt('Enter the ISBN : ');
  let authors = prompt('Enter the Author Email : ');
  let description = prompt('Enter the description :')
  let BookObj = {
    title,
    isbn,
    authors,
    description
  }
  mergedArray.push(BookObj)
  booksArray.push(BookObj)
  console.log("Book added successfully")
}

function addMagzin() {
  let title = prompt('Enter the Magzine Name : ');
  let isbn = prompt('Enter the ISBN : ');
  let authors = prompt('Enter the Author Email : ');
  let magzineObj = {
    title,
    isbn,
    authors
  }
  mergedArray.push(magzineObj)
  magzineArray.push(magzineObj)
  console.log("Magzine added successfully")
  return "Magzine added successfully"
}

async function exportBookAndMgazine() {
  const book = new ObjectsToCsv(booksArray)
  const magzine = new ObjectsToCsv(magzineArray)
  await book.toDisk('./book.csv', { append: true })
  await magzine.toDisk('./magzine.csv', { append: true })
}




async function main() {
  await getBooksAndMagezin()
  let menu = `
              1. Get All Books And Magzine
              2. Find a book or magazine by its ISBN.
              3. Find all books and magazines by their authors email.
              4. Get Books and Magzine by sort
              5. Add Book 
              6. Add Magzine
              7. Export Book and magzine`;
  
  
  
  
  console.log(menu);
  while (true) {
    
    let input = prompt('Eneter 1 - 7: ');
    key = Number(input);


    switch (key) {
      case 1:
        console.log("hello")
        displayBooksAndMgazine()
        break;
      case 2:
        let isbn = prompt('Enter the ISBN number: ');
        findBookAndMagzineByISBN(isbn)
      case 3:
        let email = prompt('Enter the email ID: ');
        await findAllBooksAndMagzineByEmail(email)
      case 4:
        sortBooksAndMagzine()
        break;
      case 5:
        addBook()
        break;
      case 6:
        addMagzin()
        break;
      case 7:
       await exportBookAndMgazine()
      break;
      default:
        console.log("Entered key is not in menu")
        break;
    }
  }
}

main()

module.exports = {addMagzin, findBookAndMagzineByISBN}